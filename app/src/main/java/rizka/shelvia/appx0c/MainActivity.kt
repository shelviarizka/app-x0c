package rizka.shelvia.appx0c

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_main.*
import mumayank.com.airlocationlibrary.AirLocation


class MainActivity : AppCompatActivity(), OnMapReadyCallback, View.OnClickListener {
    var gps : AirLocation? = null
    var map : GoogleMap? = null
    lateinit var  fragMap : SupportMapFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(rizka.shelvia.appx0c.R.layout.activity_main)
        fragMap = supportFragmentManager.findFragmentById(rizka.shelvia.appx0c.R.id.data) as SupportMapFragment
        fragMap.getMapAsync(this)
        tambah.setOnClickListener(this)
//      key  AIzaSyA5I5ycmLp_l65p6mYq0mtMK_qKDJsR7ZQ

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        gps?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onClick(v: View?) {
        gps = AirLocation(this,true,true,
            object : AirLocation.Callbacks{
                override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                    Toast.makeText(this@MainActivity, "Gagal Mendapatkan Lokasi",Toast.LENGTH_SHORT).show()
                    isi.setText("gagal Mendapat kan lokasi")
                }

                override fun onSuccess(location: Location) {
                    val ll = LatLng(location.latitude,location.longitude)
                    map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                    isi.setText("Posisi Saya LAT = ${location.latitude}\nLONG = ${location.longitude}")
                }
            }
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        gps?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onMapReady(p0: GoogleMap?) {
        map = p0
        if (map!=null){
            gps = AirLocation(this,true,true,
                object : AirLocation.Callbacks{
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(this@MainActivity, "Gagal Mendapatkan Lokasi",Toast.LENGTH_SHORT).show()
                        isi.setText("gagal Mendapat kan lokasi")
                    }

                    override fun onSuccess(location: Location) {
                        val ll = LatLng(location.latitude,location.longitude)
                        map!!.addMarker(MarkerOptions().position(ll).title("posisi saya"))
                        map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                        isi.setText("Posisi Saya LAT = ${location.latitude}\nLONG = ${location.longitude}")
                    }
                }
            )
        }
    }
}
